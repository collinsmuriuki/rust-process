use std::thread;
use std::sync::mpsc;
use std::process::{Stdio,Command};
use std::io::{BufReader,BufRead};

enum Message {
    Data(String),
    Stop,
}

fn main() {
    // channel for sending messages from child to parent
    let (child_sender,parent_receiver) = mpsc::channel();

    // channel for sending messages from parent to child
    let (parent_sender,child_receiver) = mpsc::channel();

    // create child thread
    thread::spawn(move || {

        // spawn shell process
        let mut child = Command::new("./loop.sh")
            .stdout(Stdio::piped())
            .spawn()
            .unwrap();

        // create BufReader to read data
        let mut child_out = BufReader::new(child.stdout.as_mut().unwrap());

        loop {
            // Terminate shell process if parent sends a stop message
            if let Ok(Message::Stop) = child_receiver.try_recv() {
                drop(child_out); // Very important
                child.kill().unwrap();
                break
            }

            // Read each line from stdout and send the Data to the parent thread.
            let mut line = String::new();
            child_out.read_line(&mut line).unwrap();
            child_sender.send(Message::Data(line)).unwrap()
        }

    });

    // Can be an infinite loop
    for _ in 0..10 {

        // Process data from child
        match parent_receiver.recv().unwrap() {
           Message::Data(s) => { println!("{}",s); },
           _ => {},
        }
    }

    // You can send a mesage to terminate the child process if you're done with the output.
    parent_sender.send(Message::Stop).unwrap();
}
